using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Security.Principal;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using Microsoft.Win32;
using ObiOne.ServiceMonitor.Model;
using ObiOne.ServiceMonitor.Properties;
using ObiOne.ServiceMonitor.Resources;
using ObiOne.ServiceMonitor.View;
using ObiOne.ServiceMonitor.View.Components;

namespace ObiOne.ServiceMonitor
{
    static class Program
    {
        static NotifyIcon _appIcon;
        static System.Timers.Timer _timer;
        static readonly List<Controller.ServiceMonitor> Services = new List<Controller.ServiceMonitor>();


        private static Profile _selectedProfile;

        private static ToolStripMenuItem _profilesItem;

        private static ContextMenuStrip _trayContextMenu;

        private static ServiceSettings _configurationSettingsDialog;

        public static Profile SelectedProfile
        {
            get
            {
                var profiles = Settings.Default.Profiles;
                if (profiles.Count == 0)
                {
                    profiles.Add(new Profile { Name = "Default" });
                }
                return _selectedProfile ?? (_selectedProfile = profiles[0]);
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            var windowsIdentity = WindowsIdentity.GetCurrent();
            if (windowsIdentity == null)
                MessageBox.Show("This application needs administrator privileges to run properly.");
            else
            {
                var principal = new WindowsPrincipal(windowsIdentity);
                var administrativeMode = principal.IsInRole(WindowsBuiltInRole.Administrator);

                if (administrativeMode)
                {
                    var appSingleTon = new Mutex(false, English.ServiceMonitor);
                    if (appSingleTon.WaitOne(0, false))
                    {
                        Application.EnableVisualStyles();
                        SystemEvents.SessionEnded += SystemEventSessionEnded;
                        _timer = new System.Timers.Timer(10000);
                        _timer.Elapsed += ServiceChecker;
                        _timer.Enabled = true;
                        BindServices();
                        IntializeIcon();
                        Application.Run();
                    }
                    appSingleTon.Close();
                }
                else
                {
                    var startInfo = new ProcessStartInfo
                    {
                        Verb = "runas",
                        FileName = Assembly.GetExecutingAssembly().CodeBase
                    };

                    try
                    {
                        Process.Start(startInfo);
                    }
                    catch
                    {
                        MessageBox.Show("This application needs administrator privileges to run properly.");
                    }
                }
            }
        }

        private static void SystemEventSessionEnded(object sender, SessionEndedEventArgs e)
        {
            _appIcon.Visible = false;
            Application.Exit();
        }

        private static void ServiceChecker(object source, ElapsedEventArgs e)
        {
            var status = 0;            
            foreach (var controller in Services)
            {
                controller.Refresh();
                UpdateServiceStatus(_appIcon.ContextMenuStrip, controller);
                if (controller.Status == ServiceControllerStatus.Running)
                {
                    status++;
                }
            }
            SetIcon(status);
        }

        private static void UpdateServiceStatus(ToolStrip mnu, Controller.ServiceMonitor controller)
        {
            foreach (var item in mnu.Items)
            {
                var menuItem = item as ServiceMenuItem;
                if (menuItem != null && menuItem.Service.DisplayName == controller.DisplayName && menuItem.Status != controller.Status)
                {
                    menuItem.Image = controller.Status == ServiceControllerStatus.Running ? Properties.Resources.GreenLight.ToBitmap() : Properties.Resources.RedLight.ToBitmap();
                    menuItem.Status = controller.Status;
                }
            }
        }

        private static void SetIcon(int status)
        {
            if (status == Services.Count)
            {
                _appIcon.Icon = Properties.Resources.On;
                _appIcon.Text = English.ServiceMonitor;
            }
            else if (status > 0)
            {
                _appIcon.Icon = Properties.Resources.Warning;
                _appIcon.Text = English.ServicesStopped;
            }
            else
            {
                _appIcon.Icon = Properties.Resources.Off;
                _appIcon.Text = English.AllServicesStopped;
            }
        }

        private static void BindServices()
        {
            Services.Clear();
            foreach (var serviceName in SelectedProfile.Services)
            {
                Services.Add(new Controller.ServiceMonitor(serviceName));
            }
        }

        private static void IntializeIcon()
        {
            _appIcon = new NotifyIcon { Visible = true };
            _trayContextMenu = new ContextMenuStrip();
            AddServicesToTrayContextMenu();
            AddProfiles();
            var configItem = new ToolStripMenuItem("Configuration");
            configItem.Click += ConfigItemClick;
            _trayContextMenu.Items.Add(configItem);
            _trayContextMenu.Items.Add("-");
            var closeItem = new ToolStripMenuItem("Exit");
            closeItem.Click += CloseItemClick;
            _trayContextMenu.Items.Add(closeItem);
            _appIcon.ContextMenuStrip = _trayContextMenu;
            _appIcon.Text = English.ServiceMonitor;
            _appIcon.DoubleClick += ConfigItemClick;

        }

        private static void AddServicesToTrayContextMenu()
        {
            var runningServices = 0;

            for (var index = 0; index < Services.Count; index++)
            {
                var serviceController = Services[index];
                serviceController.Refresh();

                var menuItem = new ServiceMenuItem(serviceController.DisplayName) { Service = serviceController };
                if (serviceController.Status == ServiceControllerStatus.Running)
                {
                    runningServices++;
                    menuItem.Image = Properties.Resources.GreenLight.ToBitmap();
                }
                else
                {
                    menuItem.Image = Properties.Resources.RedLight.ToBitmap();
                }
                menuItem.Status = serviceController.Status;
                menuItem.Click += ServiceItemClick;
                _trayContextMenu.Items.Insert(index, menuItem);
            }
            
            SetIcon(runningServices);
            
            if (Services.Count > 0) _trayContextMenu.Items.Insert(Services.Count, new ToolStripSeparator());
        }

        private static void ServiceItemClick(object sender, EventArgs e)
        {
            var menuItem = sender as ServiceMenuItem;
            if (menuItem != null)
            {
                menuItem.Service.Refresh();
                if (menuItem.Service.Status == ServiceControllerStatus.Running)
                {
                    menuItem.Service.Stop();
                }
                else
                {
                    menuItem.Service.Start();
                }
                menuItem.Image = Properties.Resources.OrangeLight.ToBitmap();
            }
        }

        private static void AddProfiles()
        {
            _profilesItem = new ToolStripMenuItem("Profiles");
            foreach (var profile in Settings.Default.Profiles)
            {
                _profilesItem.DropDownItems.Add(profile.Name, SelectedProfile == profile ? Properties.Resources.Check : null, ChangeProfile);
            }
            _trayContextMenu.Items.Add(_profilesItem);
        }

        private static void ChangeProfile(object sender, EventArgs e)
        {
            var profile = sender as ToolStripItem;
            if (profile == null)
            {
                MessageBox.Show(Properties.Resources.Internal_Error_Text, Properties.Resources.Error_Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (String.Compare(profile.Text, SelectedProfile.Name, StringComparison.OrdinalIgnoreCase) == 0)
            {
                return;
            }
            profile.Image = Properties.Resources.Check;
            foreach (ToolStripItem item in _profilesItem.DropDownItems)
            {
                if (String.CompareOrdinal(item.Text, SelectedProfile.Name) == 0)
                {
                    item.Image = null;
                    break;
                }
            }
            foreach (var p in Settings.Default.Profiles)
            {
                if (String.CompareOrdinal(p.Name, profile.Text) == 0)
                {
                    _selectedProfile = p;
                    break;
                }
            }
            for (var i = 0; i < Services.Count; i++)
            {
                _trayContextMenu.Items.RemoveAt(0);
            }
            if (Services.Count > 0)
            {
                _trayContextMenu.Items.RemoveAt(0);
            }
            BindServices();
            AddServicesToTrayContextMenu();
        }

        private static void ConfigItemClick(object sender, EventArgs e)
        {
            if (_configurationSettingsDialog != null)
            {
                return;
            }
            _configurationSettingsDialog = new ServiceSettings(SelectedProfile);
            _configurationSettingsDialog.ShowDialog();
            lock (Services)
            {
                Services.Clear();
                _selectedProfile = null;
                foreach (var serviceName in SelectedProfile.Services)
                {
                    Services.Add(new Controller.ServiceMonitor(serviceName));
                }
            }
            _appIcon.Dispose();
            IntializeIcon();               
            _configurationSettingsDialog.Dispose();
            _configurationSettingsDialog = null;
        }

        private static void CloseItemClick(object sender, EventArgs e)
        {
            _appIcon.Visible = false;
            Application.Exit();
        }
    }
}