﻿using System;
using System.ServiceProcess;

namespace ObiOne.ServiceMonitor.Controller
{
    public class ServiceMonitor : IDisposable
    {
        private readonly string _name;
        private ServiceController _serviceController;

        public ServiceControllerStatus Status
        {
            get
            {
                try
                {
                    if (_serviceController != null)
                    {
                        return _serviceController.Status;
                    }
                    _serviceController = new ServiceController(_name);
                    return _serviceController.Status;
                }
                catch (InvalidOperationException)
                {
                    return ServiceControllerStatus.Stopped;
                }
            }
        }

        public string DisplayName
        {
            get
            {
                try
                {
                    if (_serviceController != null)
                    {
                        return _serviceController.DisplayName;
                    }
                    _serviceController = new ServiceController(_name);
                    return _serviceController.DisplayName;
                }
                catch (InvalidOperationException)
                {
                    return _name;
                }
            }
        }

        public ServiceMonitor(string name)
        {
            if (name == null) throw new ArgumentNullException("name");
            _name = name;
            try
            {
                _serviceController = new ServiceController(name);
            }
            catch
            {
            }
        }

        public void Refresh()
        {
            try
            {
                if (_serviceController != null)
                {
                    _serviceController.Refresh();
                }
                else
                {
                    _serviceController = new ServiceController(_name);
                }
            }
            catch (InvalidOperationException)
            {
            }
        }

        public void Stop()
        {
            try
            {
                if (_serviceController != null)
                {
                    _serviceController.Stop();
                }
                else
                {
                    _serviceController = new ServiceController(_name);
                    _serviceController.Stop();
                }
            }
            catch (InvalidOperationException)
            {
            }
        }

        public void Start()
        {
            try
            {
                if (_serviceController != null)
                {
                    _serviceController.Start();
                }
                else
                {
                    _serviceController = new ServiceController(_name);
                    _serviceController.Start();
                }
            }
            catch (InvalidOperationException)
            {
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (_serviceController != null)
                {
                    _serviceController.Dispose();
                    _serviceController = null;
                }
            }
        }

        ~ServiceMonitor()
        {
            Dispose(false);
        }
    }
}