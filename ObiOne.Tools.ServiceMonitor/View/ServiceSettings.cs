﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.ServiceProcess;
using System.Windows.Forms;
using Microsoft.Win32;
using ObiOne.ServiceMonitor.Model;
using ObiOne.ServiceMonitor.Resources;
using ObiOne.ServiceMonitor.View.Components;

namespace ObiOne.ServiceMonitor.View
{
    public partial class ServiceSettings : Form
    {
        private Profile _selectedProfile;

        private bool _ignoreIndexChange;

        private ListViewColumnSorter _lstColumnSorter;
        private Dictionary<System.Windows.Forms.View, KeyValuePair<System.Windows.Forms.View, Bitmap>> _viewStyleCycle;

        public ServiceSettings(Profile selectedProfile)
        {
            if (selectedProfile == null) throw new ArgumentNullException("selectedProfile");

            _selectedProfile = selectedProfile;
            InitializeComponent();
            UpdateDropDown();
            PopulateListBoxes();
            chkStartup.Checked = Properties.Settings.Default.StartOnWindowsStartup;

            _viewStyleCycle = new Dictionary<System.Windows.Forms.View, KeyValuePair<System.Windows.Forms.View, Bitmap>>
            {
                {System.Windows.Forms.View.Details, new KeyValuePair<System.Windows.Forms.View, Bitmap>(System.Windows.Forms.View.List, Properties.Resources.list)},
                {System.Windows.Forms.View.List, new KeyValuePair<System.Windows.Forms.View, Bitmap>(System.Windows.Forms.View.Tile, Properties.Resources.tile)},
                {System.Windows.Forms.View.Tile, new KeyValuePair<System.Windows.Forms.View, Bitmap>(System.Windows.Forms.View.LargeIcon, Properties.Resources.large)},
                {System.Windows.Forms.View.LargeIcon, new KeyValuePair<System.Windows.Forms.View, Bitmap>(System.Windows.Forms.View.Details, Properties.Resources.details)},
            };
        }

        private void UpdateDropDown()
        {            
            lock (_selectedProfile)
            {
                _ignoreIndexChange = true;
                var selectedIndex = 0;
                var settings = Properties.Settings.Default;

                for (var i = 0; i < settings.Profiles.Count; i++)
                {
                    if (settings.Profiles[i] != _selectedProfile) continue;
                    selectedIndex = i;
                    break;
                }

                cbxProfiles.DataSource = null;
                cbxProfiles.DataSource = settings.Profiles;
                cbxProfiles.DisplayMember = "Name";
                cbxProfiles.SelectedItem = _selectedProfile;
                cbxProfiles.SelectedText = _selectedProfile.Name;
                cbxProfiles.SelectedIndex = selectedIndex;
                cbxProfiles.Refresh();
                btnDelete.Visible = settings.Profiles.Count > 1;
                _ignoreIndexChange = false;
            }                        
        }

        private void PopulateListBoxes()
        {            
            var services = new List<ServiceController>(ServiceController.GetServices());

            lstServices.Items.Clear();
            lstSelectedServices.Items.Clear();

            foreach (var service in services)
            {
                if (_selectedProfile.Services.Contains(service.ServiceName))
                {
                    lstSelectedServices.Items.Add(new ListServiceItem(service));
                }
                else
                {
                    lstServices.Items.Add(new ListServiceItem(service));
                }
            }
        }

        private void Column_OnClick(object objectSender, ColumnClickEventArgs columnClickEventArgs)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (columnClickEventArgs.Column == _lstColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                _lstColumnSorter.Order = _lstColumnSorter.Order == SortOrder.Ascending ? SortOrder.Descending : SortOrder.Ascending;
            } else
            {
                // Set the column number that is to be sorted; default to ascending.
                _lstColumnSorter.SortColumn = columnClickEventArgs.Column;
                _lstColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            var listViewControl = ((ListView)objectSender);
            listViewControl.Sort();
            ListViewExtensions.SetSortIcon(listViewControl, Convert.ToInt32(_lstColumnSorter.SortColumn), _lstColumnSorter.Order);
        }

        private void BtnOk_OnClick(object sender, EventArgs e)
        {
            SaveSelectedServices();
            var settings = Properties.Settings.Default;
            if (settings.StartOnWindowsStartup != chkStartup.Checked)
            {
                if (SetStartup(chkStartup.Checked))
                {
                    settings.StartOnWindowsStartup = chkStartup.Checked;
                }
                else
                {
                    chkStartup.Checked = settings.StartOnWindowsStartup;
                    return;
                }   
            }            
            settings.Save();
            Close();
        }

        private void SaveSelectedServices()
        {
            _selectedProfile.Services.Clear();
            foreach (ListServiceItem item in lstSelectedServices.Items)
            {
                _selectedProfile.Services.Add(item.Service.ServiceName);
            }
        }

        private static bool SetStartup(bool status)
        {
            var rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (rk!=null)
            {
                if (status)
                {
                    rk.SetValue("Windows Service Monitor", Application.ExecutablePath);
                }
                else
                {
                    rk.DeleteValue("Windows Service Monitor", false);
                }
            }    
            else
            {
                MessageBox.Show(English.NoAccessToRegistry, English.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void BtnCancel_OnClick(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reload();
            Close();
        }

        private void ServiceSettingsResize(object sender, EventArgs e)
        {
            lstServices.Width = (Width - 80 - 24)/2;
            lstSelectedServices.Width = lstServices.Width;
            lstServices.Height = Height - 152;
            lstSelectedServices.Height = lstServices.Height;
            lbServicesSelected.Left = lstServices.Width + 80;
            lstSelectedServices.Left = lbServicesSelected.Left;
            btnRight.Left = Width/2 - 22;
            btnLeft.Left = btnRight.Left;
            btnRight.Top = lstServices.Top + lstServices.Height / 2 - 31;
            btnLeft.Top = btnRight.Top + 37;            
        }

        private void CbxProfiles_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxProfiles.SelectedIndex != -1 && !_ignoreIndexChange)
            {
                SaveSelectedServices();
                _selectedProfile = Properties.Settings.Default.Profiles[cbxProfiles.SelectedIndex];    
                PopulateListBoxes();
            }            
        }

        private void LstServices_OnDoubleClick(object sender, EventArgs e)
        {
            BtnRight_OnClick(sender,e);
        }

        private void BtnRight_OnClick(object sender, EventArgs e)
        {            
            foreach (ListServiceItem selectedItem in lstServices.SelectedItems)
            {
                lstServices.Items.Remove(selectedItem);
                if (!lstSelectedServices.Items.Contains(selectedItem)) lstSelectedServices.Items.Add(selectedItem);
            }
        }

        private void LstSelectedServices_OnDoubleClick(object sender, EventArgs e)
        {
            BtnLeft_OnClick(sender,e);
        }

        private void BtnLeft_OnClick(object sender, EventArgs e)
        {
            foreach (ListServiceItem selectedItem in lstSelectedServices.SelectedItems)
            {
                lstSelectedServices.Items.Remove(selectedItem);
                if (!lstServices.Items.Contains(selectedItem)) lstServices.Items.Add(selectedItem);
            }
        }


        private void BtnNew_OnClick(object sender, EventArgs e)
        {
            var dlg = new InputText
            {
                Title = "Enter Profile Name",
                TextLabel = "Profile Name"
            };
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                if (!IsValidName(dlg.TextValue))
                {
                    MessageBox.Show(Properties.Resources.Profile_name_should_be_unique_Error_Text, Properties.Resources.Error_Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                SaveSelectedServices();
                _selectedProfile = new Profile {Name = dlg.TextValue};
                Properties.Settings.Default.Profiles.Add(_selectedProfile);                
                PopulateListBoxes();
                UpdateDropDown();              
            }
        }

        private bool IsValidName(string newName)
        {
            foreach (var p in Properties.Settings.Default.Profiles)
            {
                if (String.CompareOrdinal(p.Name, newName) == 0)
                {
                    return false;
                }
            }
            return true;
        }

        private void BtnRename_OnClick(object sender, EventArgs e)
        {
            var dlg = new InputText
            {
                Title = "Enter New Profile Name",
                TextLabel = "Profile Name",
                TextValue =  _selectedProfile.Name
            };
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                _selectedProfile.Name = dlg.TextValue;                                
                UpdateDropDown();
            }
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            var selectedIndex = cbxProfiles.SelectedIndex;
            selectedIndex = selectedIndex > 0 ? selectedIndex - 1 : 0;
            _selectedProfile = Properties.Settings.Default.Profiles[selectedIndex];
            Properties.Settings.Default.Profiles.Remove((Profile)cbxProfiles.SelectedItem);            
            PopulateListBoxes();
            UpdateDropDown();
        }

        private void BtnViewStyle_OnClick(object sender, EventArgs e)
        {
            BtnViewStyle.Image = _viewStyleCycle[lstServices.View].Value;
            lstServices.View = _viewStyleCycle[lstServices.View].Key;
            lstSelectedServices.View = _viewStyleCycle[lstSelectedServices.View].Key;
        }

        private void ServiceSettings_Load(object sender, EventArgs e)
        {
            _lstColumnSorter = new ListViewColumnSorter();
            lstServices.ListViewItemSorter = _lstColumnSorter;
            lstSelectedServices.ListViewItemSorter = _lstColumnSorter;
        }
    }
}
