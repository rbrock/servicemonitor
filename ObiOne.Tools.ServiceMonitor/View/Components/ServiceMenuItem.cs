﻿using System;
using System.ServiceProcess;
using System.Windows.Forms;

namespace ObiOne.ServiceMonitor.View.Components
{
    public class ServiceMenuItem : ToolStripMenuItem
    {
        public Controller.ServiceMonitor Service;

        public ServiceControllerStatus Status = ServiceControllerStatus.Stopped;

        public ServiceMenuItem(string text) : base(text)
        {
            if (text == null) throw new ArgumentNullException("text");
        }
    }
}