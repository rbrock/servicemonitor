﻿using System;
using System.Runtime.Serialization;
using System.ServiceProcess;
using System.Windows.Forms;
using ObiOne.ServiceMonitor.Model;

namespace ObiOne.ServiceMonitor.View.Components
{
    [Serializable]
    public class ListServiceItem : ListViewItem
    {
        public readonly ServiceController Service;

        public ListServiceItem(ServiceController service)
        {
            if (service == null)
                throw new ArgumentNullException("service");

            ImageIndex = 0;
            Service = service;
            var serviceDetail = new ServiceDetail(service.ServiceName);

            Text = serviceDetail.DisplayName;
            SubItems.Add(serviceDetail.Description);
            SubItems.Add(serviceDetail.State.ToString());
            SubItems.Add(serviceDetail.StartMode.ToString());
        }

        protected ListServiceItem(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
