﻿namespace ObiOne.ServiceMonitor.View
{
    partial class ServiceSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServiceSettings));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.lbServices = new System.Windows.Forms.Label();
            this.lbServicesSelected = new System.Windows.Forms.Label();
            this.chkStartup = new System.Windows.Forms.CheckBox();
            this.lbProfile = new System.Windows.Forms.Label();
            this.cbxProfiles = new System.Windows.Forms.ComboBox();
            this.btnRename = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lstServices = new System.Windows.Forms.ListView();
            this.colServiceName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colServiceDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colServiceStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colServiceStartupMode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.largeImageList = new System.Windows.Forms.ImageList(this.components);
            this.smallImageList = new System.Windows.Forms.ImageList(this.components);
            this.BtnViewStyle = new System.Windows.Forms.Button();
            this.lstSelectedServices = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(412, 374);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_OnClick);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(497, 374);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_OnClick);
            // 
            // btnRight
            // 
            this.btnRight.Image = global::ObiOne.ServiceMonitor.Properties.Resources.ArrowRight;
            this.btnRight.Location = new System.Drawing.Point(274, 185);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(35, 25);
            this.btnRight.TabIndex = 3;
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.BtnRight_OnClick);
            // 
            // btnLeft
            // 
            this.btnLeft.Image = global::ObiOne.ServiceMonitor.Properties.Resources.ArrowLeft;
            this.btnLeft.Location = new System.Drawing.Point(274, 222);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(35, 25);
            this.btnLeft.TabIndex = 3;
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.BtnLeft_OnClick);
            // 
            // lbServices
            // 
            this.lbServices.AutoSize = true;
            this.lbServices.Location = new System.Drawing.Point(12, 46);
            this.lbServices.Name = "lbServices";
            this.lbServices.Size = new System.Drawing.Size(51, 13);
            this.lbServices.TabIndex = 4;
            this.lbServices.Text = "Services:";
            // 
            // lbServicesSelected
            // 
            this.lbServicesSelected.AutoSize = true;
            this.lbServicesSelected.Location = new System.Drawing.Point(322, 46);
            this.lbServicesSelected.Name = "lbServicesSelected";
            this.lbServicesSelected.Size = new System.Drawing.Size(101, 13);
            this.lbServicesSelected.TabIndex = 4;
            this.lbServicesSelected.Text = "Monitored Services:";
            // 
            // chkStartup
            // 
            this.chkStartup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkStartup.AutoSize = true;
            this.chkStartup.Location = new System.Drawing.Point(15, 377);
            this.chkStartup.Name = "chkStartup";
            this.chkStartup.Size = new System.Drawing.Size(209, 17);
            this.chkStartup.TabIndex = 5;
            this.chkStartup.Text = "Start automatically on Windows startup";
            this.chkStartup.UseVisualStyleBackColor = true;
            // 
            // lbProfile
            // 
            this.lbProfile.AutoSize = true;
            this.lbProfile.Location = new System.Drawing.Point(12, 16);
            this.lbProfile.Name = "lbProfile";
            this.lbProfile.Size = new System.Drawing.Size(39, 13);
            this.lbProfile.TabIndex = 6;
            this.lbProfile.Text = "Profile:";
            // 
            // cbxProfiles
            // 
            this.cbxProfiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxProfiles.FormattingEnabled = true;
            this.cbxProfiles.Location = new System.Drawing.Point(61, 13);
            this.cbxProfiles.Name = "cbxProfiles";
            this.cbxProfiles.Size = new System.Drawing.Size(121, 21);
            this.cbxProfiles.TabIndex = 7;
            this.cbxProfiles.SelectedIndexChanged += new System.EventHandler(this.CbxProfiles_OnSelectedIndexChanged);
            // 
            // btnRename
            // 
            this.btnRename.Location = new System.Drawing.Point(189, 13);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(73, 23);
            this.btnRename.TabIndex = 8;
            this.btnRename.Text = "Rename";
            this.btnRename.UseVisualStyleBackColor = true;
            this.btnRename.Click += new System.EventHandler(this.BtnRename_OnClick);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(268, 13);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(55, 23);
            this.btnNew.TabIndex = 9;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.BtnNew_OnClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(329, 13);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(55, 23);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.BtnDeleteClick);
            // 
            // lstServices
            // 
            this.lstServices.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colServiceName,
            this.colServiceDescription,
            this.colServiceStatus,
            this.colServiceStartupMode});
            this.lstServices.FullRowSelect = true;
            this.lstServices.LargeImageList = this.largeImageList;
            this.lstServices.Location = new System.Drawing.Point(12, 71);
            this.lstServices.Name = "lstServices";
            this.lstServices.Size = new System.Drawing.Size(250, 290);
            this.lstServices.SmallImageList = this.smallImageList;
            this.lstServices.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstServices.TabIndex = 0;
            this.lstServices.UseCompatibleStateImageBehavior = false;
            this.lstServices.View = System.Windows.Forms.View.Details;
            this.lstServices.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.Column_OnClick);
            this.lstServices.DoubleClick += new System.EventHandler(this.LstServices_OnDoubleClick);
            // 
            // colServiceName
            // 
            this.colServiceName.Text = "Name";
            this.colServiceName.Width = 150;
            // 
            // colServiceDescription
            // 
            this.colServiceDescription.Text = "Description";
            // 
            // colServiceStatus
            // 
            this.colServiceStatus.Text = "Status";
            // 
            // colServiceStartupMode
            // 
            this.colServiceStartupMode.Text = "Startup Mode";
            // 
            // largeImageList
            // 
            this.largeImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("largeImageList.ImageStream")));
            this.largeImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.largeImageList.Images.SetKeyName(0, "settings.ico");
            // 
            // smallImageList
            // 
            this.smallImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("smallImageList.ImageStream")));
            this.smallImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.smallImageList.Images.SetKeyName(0, "settings.ico");
            // 
            // BtnViewStyle
            // 
            this.BtnViewStyle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnViewStyle.Image = global::ObiOne.ServiceMonitor.Properties.Resources.details;
            this.BtnViewStyle.Location = new System.Drawing.Point(548, 16);
            this.BtnViewStyle.Name = "BtnViewStyle";
            this.BtnViewStyle.Size = new System.Drawing.Size(24, 24);
            this.BtnViewStyle.TabIndex = 11;
            this.BtnViewStyle.UseVisualStyleBackColor = true;
            this.BtnViewStyle.Click += new System.EventHandler(this.BtnViewStyle_OnClick);
            // 
            // lstSelectedServices
            // 
            this.lstSelectedServices.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lstSelectedServices.FullRowSelect = true;
            this.lstSelectedServices.LargeImageList = this.largeImageList;
            this.lstSelectedServices.Location = new System.Drawing.Point(322, 71);
            this.lstSelectedServices.Name = "lstSelectedServices";
            this.lstSelectedServices.Size = new System.Drawing.Size(250, 290);
            this.lstSelectedServices.SmallImageList = this.smallImageList;
            this.lstSelectedServices.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstSelectedServices.TabIndex = 1;
            this.lstSelectedServices.UseCompatibleStateImageBehavior = false;
            this.lstSelectedServices.View = System.Windows.Forms.View.Details;
            this.lstSelectedServices.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.Column_OnClick);
            this.lstSelectedServices.DoubleClick += new System.EventHandler(this.LstSelectedServices_OnDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Description";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Status";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Startup Mode";
            // 
            // ServiceSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 409);
            this.Controls.Add(this.lstSelectedServices);
            this.Controls.Add(this.BtnViewStyle);
            this.Controls.Add(this.lstServices);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnRename);
            this.Controls.Add(this.cbxProfiles);
            this.Controls.Add(this.lbProfile);
            this.Controls.Add(this.chkStartup);
            this.Controls.Add(this.lbServicesSelected);
            this.Controls.Add(this.lbServices);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ServiceSettings";
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.ServiceSettings_Load);
            this.Resize += new System.EventHandler(this.ServiceSettingsResize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Label lbServices;
        private System.Windows.Forms.Label lbServicesSelected;
        private System.Windows.Forms.CheckBox chkStartup;
        private System.Windows.Forms.Label lbProfile;
        private System.Windows.Forms.ComboBox cbxProfiles;
        private System.Windows.Forms.Button btnRename;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ListView lstServices;
        private System.Windows.Forms.ColumnHeader colServiceName;
        private System.Windows.Forms.ColumnHeader colServiceDescription;
        private System.Windows.Forms.ColumnHeader colServiceStatus;
        private System.Windows.Forms.ColumnHeader colServiceStartupMode;
        private System.Windows.Forms.Button BtnViewStyle;
        private System.Windows.Forms.ImageList smallImageList;
        private System.Windows.Forms.ImageList largeImageList;
        private System.Windows.Forms.ListView lstSelectedServices;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
    }
}