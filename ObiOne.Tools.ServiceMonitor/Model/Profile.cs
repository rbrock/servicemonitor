﻿using System;
using System.Collections.Generic;

namespace ObiOne.ServiceMonitor.Model
{
    [Serializable]
    public class Profile
    {
        private List<string> _services;
        public string Name { get; set; }

        public List<string> Services
        {
            get { return _services ?? (_services = new List<string>()); }
            set { _services = value; }
        }
    }
}
