﻿using System;
using System.ComponentModel;
using System.Management;
using System.ServiceProcess;

namespace ObiOne.ServiceMonitor.Model
{
    public class ServiceDetail
    {
        public string Name { get; set; }

        public string Caption { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public ServiceStartMode StartMode { get; set; }

        public string StartName { get; set; }

        public bool Started { get; set; }

        public ServiceControllerStatus State { get; set; }

        public ServiceDetail(string serviceName)
        {
            if (serviceName == null) throw new ArgumentNullException("serviceName");

            var wmiService = new ManagementObject(string.Format("Win32_Service.Name='{0}'", serviceName));
            wmiService.Get();

            #if DEBUG
            var detailList = new Dictionary<string, string>();
            foreach (var property in wmiService.Properties)
            {
                detailList.Add(property.Name, property.Value != null ? property.Value.ToString() : string.Empty);
            }            
            #endif

            Name = wmiService.Properties["Name"].Value as string;
            Caption = wmiService.Properties["Caption"].Value as string;
            DisplayName = wmiService.Properties["DisplayName"].Value as string;
            Description = wmiService.Properties["Description"].Value as string;

            switch (wmiService.Properties["StartMode"].Value as string)
            {
                case "Auto":
                    StartMode = ServiceStartMode.Automatic;
                    break;
                case "Disabled":
                    StartMode = ServiceStartMode.Disabled;
                    break;
                case "Manual":
                    StartMode = ServiceStartMode.Manual;
                    break;
                default:
                    throw new InvalidEnumArgumentException();
            }


            StartName = wmiService.Properties["StartName"].Value as string;
            Started = (bool)wmiService.Properties["Started"].Value;

            switch (wmiService.Properties["State"].Value as string)
            {
                case "ContinuePending":
                    State = ServiceControllerStatus.ContinuePending;
                    break;
                case "PausePending":
                    State = ServiceControllerStatus.PausePending;
                    break;
                case "Paused":
                    State = ServiceControllerStatus.Paused;
                    break;
                case "Running":
                    State = ServiceControllerStatus.Running;
                    break;
                case "StartPending":
                    State = ServiceControllerStatus.StartPending;
                    break;
                case "StopPending":
                    State = ServiceControllerStatus.StopPending;
                    break;
                case "Stopped":
                    State = ServiceControllerStatus.Stopped;
                    break;
                default:
                    throw new InvalidEnumArgumentException();
            }
        }

        //{[Status, OK]}

        //{[PathName, C:\Windows\system32\atiesrxx.exe]}

        //{[AcceptPause, False]}
        //{[AcceptStop, True]}
        //{[CheckPoint, 0]}
        //{[CreationClassName, Win32_Service]}
        //{[DesktopInteract, False]}
        //{[ErrorControl, Normal]}
        //{[ExitCode, 0]}
        //{[InstallDate, ]}	

        //{[ProcessId, 244]}
        //{[ServiceSpecificExitCode, 0]}
        //{[ServiceType, Own Process]}
        //{[SystemCreationClassName, Win32_ComputerSystem]}
        //{[SystemName, DEATHSTAR]}
        //{[TagId, 0]}
        //{[WaitHint, 0]}
    }
}
