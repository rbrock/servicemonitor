﻿using System.Collections.Generic;
using System.Configuration;
using ObiOne.ServiceMonitor.Model;

namespace ObiOne.ServiceMonitor.Properties
{
    internal sealed class Settings : ApplicationSettingsBase
    {
        private static Settings _default;

        public static Settings Default 
        { 
            get
            {
                if (_default == null)
                {
                    _default = new Settings();
                    _default.Reload();
                }
                return _default;
            }            
        }

        [UserScopedSetting]
        [SettingsSerializeAs(SettingsSerializeAs.Xml)]
        [DefaultSettingValue("")]
        public List<Profile> Profiles
        {
            get
            {
                return (List<Profile>)(this["Profiles"] ?? (this["Profiles"] = new List<Profile>()));
            }
            set
            {
                this["Profiles"] = value;
            }
        }

        [UserScopedSetting]
        [SettingsSerializeAs(SettingsSerializeAs.Xml)]
        [DefaultSettingValue("")]
        public bool StartOnWindowsStartup
        {
            get
            {
                return (bool)(this["StartOnWindowsStartup"] ?? (this["StartOnWindowsStartup"] = false));
            }
            set
            {
                this["StartOnWindowsStartup"] = value;
            }
        }
    }
}